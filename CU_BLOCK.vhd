library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY BitSlice IS
    PORT
    (
        A        :    IN STD_LOGIC_VECTOR(3 downto 0);
        B        :    IN STD_LOGIC_VECTOR(3 downto 0);
        sel      :    IN STD_LOGIC_VECTOR(2 downto 0);
        CiBi     :    IN STD_LOGIC;
        S        :    OUT STD_LOGIC_VECTOR(3 downto 0);
        CoBo     :    OUT STD_LOGIC
    );
END BitSlice;

ARCHITECTURE BitSlice_architecture OF BitSlice IS

BEGIN    
    process (A, B, CiBi, Sel)
          variable temp_add_1, temp_add_2, temp_add_3, temp_add_4, temp_add_5 : STD_LOGIC;
            variable temp_sub_1, temp_sub_2, temp_sub_3, temp_sub_4 : STD_LOGIC;
             variable temp_C : STD_LOGIC_VECTOR(3 downto 0);
    begin
        -- ADD
        if Sel = "00" then
                    --Convert std_logic C to std_logic_vector;
                    temp_C := (others => CiBi);
                    S <= (A + B) + temp_C after 3 ns;
            --S <= (A XOR B) XOR CiBi after 3 ns;
            temp_add_1 := ( (A(0) AND B(0)) AND not(CiBi) );
            temp_add_2 := ( (A(1) AND B(1)) AND not(CiBi) );
            temp_add_3 := ( (A(2) AND B(2)) AND not(CiBi) );
            temp_add_4 := ( (A(3) AND B(3)) AND not(CiBi) );
            --temp_add_5 := (A OR B) AND CiBi;
            CoBo <= temp_add_1 OR temp_add_2 OR temp_add_3 OR temp_add_4  after 2 ns;
            
        -- SUB
        elsif Sel = "01" then
                    temp_C := (others => CiBi);
                    S <= (A - B) + temp_C after 3 ns;
            temp_sub_1 := ( NOT(A(0)) AND B(0) );
            temp_sub_2 := ( (NOT(A(1)) AND B(1)) OR (A(1) AND NOT(B(1))) ) AND CiBi;
            temp_sub_3 := ( (NOT(A(2)) AND B(2)) OR (A(2) AND NOT(B(2))) ) AND CiBi;
            temp_sub_4 := ( (NOT(A(3)) AND B(3)) OR (A(3) AND NOT(B(3))) ) AND CiBi;
            CoBo <= temp_sub_1 OR temp_sub_2 OR temp_sub_3 OR temp_sub_4 after 2 ns;
            
        --EXOR
        elsif Sel = "10" then
            S <= A XOR B after 3 ns;
            CoBo <= '0' after 2 ns;
            
        --Inc_A
        else
            S <= ("000" & CiBi)  XOR A after 3 ns;
            CoBo <= CiBi AND A(0) after 2 ns; -- only A(0) is relevant for carry out
        end if;
    end process;
END BitSlice_architecture;