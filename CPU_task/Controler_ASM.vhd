LIBRARY ieee;
USE ieee.std_logic_1164.all;

--  Entity Declaration
ENTITY controller IS
    PORT
    (
        nrst    : IN STD_LOGIC;
        clk     : IN STD_LOGIC;
        opcode  : IN STD_LOGIC_VECTOR(2 downto 0);
        pc_en   : OUT STD_LOGIC;
        mux_sel : OUT STD_LOGIC;
        rega_ld : OUT STD_LOGIC;
        regb_ld : OUT STD_LOGIC;
        alu_sel : OUT STD_LOGIC_VECTOR(1 downto 0);
        acu_ld  : OUT STD_LOGIC
    );
END controller;

--  Architecture Body
ARCHITECTURE controller_architecture OF controller IS
    TYPE state IS (Su, Sx, S0, S1, S2, S3, S4, S5, S6, S7); -- Su = State uninitialized 
    SIGNAL presentstate, nextstate : state;
BEGIN
    Next_state_decoder: 
    PROCESS(presentstate, opcode)
        VARIABLE n_s : state;
    BEGIN
        CASE presentstate IS
            WHEN S0 => 
                CASE opcode IS
                    WHEN "001" => n_s := S1; -- multi
                    WHEN "010" => n_s := S2; -- LOAD A
                    WHEN "011" => n_s := S3; -- LOAD B
                    WHEN "100" => n_s := S4; -- add
                    WHEN "101" => n_s := S5; -- INC
                    WHEN "110" => n_s := S6; -- SHR
                    WHEN "111" => n_s := S7; -- SHL
                    WHEN "000" => n_s := S0; -- IDLE
                    WHEN OTHERS => n_s := S0;
                END CASE;
            WHEN S1 => n_s := S0;
            WHEN S2 => n_s := S0;
            WHEN S3 => n_s := S0;
            WHEN S4 => n_s := S0;
            WHEN S5 => n_s := S0;
            WHEN S6 => n_s := S0;
            WHEN S7 => n_s := S0;
            WHEN OTHERS => n_s := S0;
        END CASE;
        nextstate <= n_s AFTER 1 ns;
    END PROCESS;

    Memory: 
    PROCESS(nrst, clk)
    BEGIN
        IF nrst = '0' THEN
            presentstate <= S0 AFTER 1 ns;
        ELSIF rising_edge(clk) THEN
            presentstate <= nextstate AFTER 1 ns;
        END IF;
    END PROCESS;

    Output_decoder: 
    PROCESS(presentstate)
        VARIABLE outp_bus : STD_LOGIC_VECTOR(6 DOWNTO 0);
    BEGIN
        CASE presentstate IS
            --                                    ┌──────pc_en
            --                                    │┌─────mux_sel
            --                                    ││┌────rega_ld
            --                                    │││┌───regb_ld
            --                                    ││││┌──alu_sel1
            --                                    │││││┌─alu_sel0
            --                                    ││││││┌acu_ld
            WHEN S0 => outp_bus := "1000000";
            WHEN S1 => outp_bus := "0000001";
            WHEN S2 => outp_bus := "0010000";
            WHEN S3 => outp_bus := "0001000";
            WHEN S4 => outp_bus := "0000001";
            WHEN S5 => outp_bus := "0101011";
            WHEN S6 => outp_bus := "0101101";
            WHEN S7 => outp_bus := "0101111";
            WHEN OTHERS => outp_bus := "0000000"; -- default case
        END CASE;
        
        -- drive all outputs
        pc_en   <= outp_bus(6) AFTER 1 ns;
        mux_sel <= outp_bus(5) AFTER 1 ns;
        rega_ld <= outp_bus(4) AFTER 1 ns;
        regb_ld <= outp_bus(3) AFTER 1 ns;
        alu_sel <= outp_bus(2 DOWNTO 1) AFTER 1 ns;
        acu_ld  <= outp_bus(0) AFTER 1 ns;
    END PROCESS;
END controller_architecture;
