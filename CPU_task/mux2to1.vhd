LIBRARY ieee;
USE ieee.std_logic_1164.all;

--  Entity Declaration

ENTITY mux2to1 IS
    PORT
    (
        in0  : IN  STD_LOGIC_VECTOR(3 downto 0);
        in1  : IN  STD_LOGIC_VECTOR(3 downto 0);
        sel  : IN  STD_LOGIC;
        outp : OUT STD_LOGIC_VECTOR(3 downto 0)
    );
END mux2to1;

--  Architecture Body

ARCHITECTURE mux2to1_architecture OF mux2to1 IS
BEGIN
    PROCESS(in0, in1, sel)
    BEGIN
        IF sel = '1' THEN
            outp <= in1;  -- If sel is 1, output in1
        ELSE
            outp <= in0;  -- If sel is 0, output in0
        END IF;
    END PROCESS;
END mux2to1_architecture;
