LIBRARY ieee;
USE ieee.std_logic_1164.all;

--  Entity Declaration for reg

ENTITY reg IS
    PORT
    (
        nrst : IN STD_LOGIC;
        clk  : IN STD_LOGIC;
        ld   : IN STD_LOGIC;
        data : IN STD_LOGIC_VECTOR(3 downto 0);
        outp : OUT STD_LOGIC_VECTOR(3 downto 0)
    );
END reg;

--  Architecture Body for reg

ARCHITECTURE reg_architecture OF reg IS
BEGIN
    PROCESS(clk, nrst)
    BEGIN
        IF nrst = '0' THEN
            outp <= (others => '0');  -- Asynchronous reset
        ELSIF rising_edge(clk) THEN
            IF ld = '1' THEN
                outp <= data;  -- Load data into register
            END IF;
        END IF;
    END PROCESS;
END reg_architecture;
