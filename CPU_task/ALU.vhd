LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all;

--  Entity Declaration
ENTITY alu IS
PORT
(
    a, b : IN STD_LOGIC_VECTOR(3 downto 0);
    sel  : IN STD_LOGIC_VECTOR(1 downto 0);
    res  : OUT STD_LOGIC_VECTOR(3 downto 0);
    ovf  : OUT STD_LOGIC;
    zero : OUT STD_LOGIC
);
END alu;

--  Architecture Body
ARCHITECTURE alu_architecture OF alu IS
    SIGNAL temp_res : STD_LOGIC_VECTOR(3 downto 0); -- Internal signal for result
BEGIN
    PROCESS(a, b, sel)
        VARIABLE temp : STD_LOGIC_VECTOR(4 downto 0); -- 5-bit temp to detect overflow
    BEGIN
        CASE sel IS
            WHEN "00" => 
                -- Addition
                temp := ('0' & a) + ('0' & b); -- Perform addition
                temp_res <= temp(3 downto 0); -- Assign the lower 4 bits to the internal result
                ovf <= temp(4); -- Overflow is the fifth bit

            WHEN "01" => 
                -- Increment B
                temp := std_logic_vector(unsigned(b) + 1); -- Increment b by 1
                temp_res <= temp(3 downto 0); -- Assign the lower 4 bits to the internal result
                ovf <= temp(4); -- Overflow is the fifth bit

            WHEN "10" => 
                -- Logical shift right
                temp_res <= '0' & b(3 downto 1); -- Shift right, MSB is 0
                ovf <= '0'; -- No overflow in shift right

            WHEN "11" => 
                -- Logical shift left
                temp_res <= std_logic_vector(unsigned(b) sll 1); -- Shift left logical
                ovf <= '0'; -- No overflow in shift left

            WHEN OTHERS =>
                -- Default case
                temp_res <= "----";
                ovf <= '0';
        END CASE;

        -- Check if the result is zero
        IF temp_res = "0000" THEN
            zero <= '1';
        ELSE
            zero <= '0';
        END IF;
    END PROCESS;

    -- Assign internal result to the output port
    res <= temp_res;
END alu_architecture;
