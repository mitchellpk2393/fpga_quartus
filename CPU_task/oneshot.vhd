LIBRARY ieee;
USE ieee.std_logic_1164.all;

--  Entity Declaration
ENTITY oneshot_block IS
    PORT
    (
        enab    : IN STD_LOGIC;
        sys_clk : IN STD_LOGIC;
        nrst    : IN STD_LOGIC;
        man_clk : OUT STD_LOGIC
    );
END oneshot_block;

--  Architecture Body
ARCHITECTURE oneshot_block_architecture OF oneshot_block IS
    TYPE state_type IS (S0, S1, S2);
    SIGNAL nextstate, presentstate : state_type;

BEGIN
    -- Next State Logic
    PROCESS(presentstate, enab)
    BEGIN
        CASE presentstate IS
            WHEN S0 =>
                IF enab = '0' THEN
                    nextstate <= S1;
                ELSE
                    nextstate <= S0;
                END IF;
            WHEN S1 =>
                nextstate <= S2;
            WHEN S2 =>
                IF enab = '1' THEN
                    nextstate <= S0;
                ELSE
                    nextstate <= S2;
                END IF;
            WHEN OTHERS =>
                nextstate <= S0;
        END CASE;
    END PROCESS;

    -- State Memory
    PROCESS(nrst, sys_clk)
    BEGIN
        IF nrst = '0' THEN
            presentstate <= S0;
        ELSIF rising_edge(sys_clk) THEN
            presentstate <= nextstate;
        END IF;
    END PROCESS;

    -- Output Logic
    PROCESS(presentstate)
    BEGIN
        CASE presentstate IS
            WHEN S1 =>
                man_clk <= '1';
            WHEN OTHERS =>
                man_clk <= '0';
        END CASE;
    END PROCESS;

END oneshot_block_architecture;
