library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CPU_Controller is
    port (
        clk        : in std_logic;
        nrst       : in std_logic;
        instruction: in std_logic_vector(6 downto 0);
        address    : out std_logic_vector(2 downto 0);
        S          : out std_logic_vector(3 downto 0);
        ovf        : out std_logic;
        zero       : out std_logic
    );
end CPU_Controller;

architecture Behavioral of CPU_Controller is
    type state_type is (RESET, IDLE, MULTIPLY, LOAD_A, LOAD_B, ADD, INCREMENT, SHIFT_RIGHT, SHIFT_LEFT);
    signal current_state, next_state: state_type;
    signal address_reg : std_logic_vector(2 downto 0);
    signal S_reg       : std_logic_vector(3 downto 0);
    signal A, B        : std_logic_vector(3 downto 0);
    signal opcode      : std_logic_vector(2 downto 0);
    signal operand     : std_logic_vector(3 downto 0);
begin

    process(clk, nrst)
    begin
        if nrst = '0' then
            current_state <= RESET;
        elsif rising_edge(clk) then
            current_state <= next_state;
        end if;
    end process;

    process(current_state, instruction)
    begin
        next_state <= current_state; -- Default to stay in the same state
        case current_state is
            when RESET =>
                address_reg <= "000";
                S_reg <= "0000";
                next_state <= IDLE;
            when IDLE =>
                address_reg <= address_reg + 1;
                opcode <= instruction(6 downto 4);
                operand <= instruction(3 downto 0);
                case opcode is
                    when "000" => next_state <= IDLE;
                    when "001" => next_state <= MULTIPLY;
                    when "010" => next_state <= LOAD_A;
                    when "011" => next_state <= LOAD_B;
                    when "100" => next_state <= ADD;
                    when "101" => next_state <= INCREMENT;
                    when "110" => next_state <= SHIFT_RIGHT;
                    when "111" => next_state <= SHIFT_LEFT;
                    when others => next_state <= IDLE;
                end case;
            when MULTIPLY =>
                S_reg <= std_logic_vector(unsigned(operand) * 3);
                next_state <= IDLE;
            when LOAD_A =>
                A <= operand;
                next_state <= IDLE;
            when LOAD_B =>
                B <= operand;
                next_state <= IDLE;
            when ADD =>
                S_reg <= std_logic_vector(unsigned(A) + unsigned(B));
                next_state <= IDLE;
            when INCREMENT =>
                S_reg <= std_logic_vector(unsigned(S_reg) + 1);
                next_state <= IDLE;
            when SHIFT_RIGHT =>
                S_reg <= '0' & S_reg(3 downto 1);
                next_state <= IDLE;
            when SHIFT_LEFT =>
                S_reg <= S_reg(2 downto 0) & '0';
                next_state <= IDLE;
            when others =>
                next_state <= IDLE;
        end case;
    end process;

    address <= address_reg;
    S <= S_reg;
    ovf <= '0';  -- Overflow detection logic not implemented
    zero <= '1' when S_reg = "0000" else '0';

end Behavioral;
