library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity addsub is
    Port (  A : in  STD_LOGIC_VECTOR(9 downto 6);
                B : in  STD_LOGIC_VECTOR(5 downto 2);
           Sel : in  STD_LOGIC;  -- SEL Control signal for subtraction
           Sum   : out STD_LOGIC_VECTOR(3 downto 0);
           Cout  : out STD_LOGIC;
              SLED    : out STD_LOGIC_VECTOR(3 downto 0)
        );      
end addsub;

architecture Behavioral of addsub is
begin
    process(A, B, Sel)
         variable temp_result : STD_LOGIC_VECTOR(4 downto 0);-- 5bit are using for overflow
    begin
        if Sel = '1' then
            temp_result := ('0' & A) - ('0' & B);--Here is unsigned numbers will or may appear
        else
            temp_result := ('0' & A) + ('0' & B);--4bit force to a 5bit (plak een 0 naar A) te get an overflow
        end if;

        Sum <= temp_result(3 downto 0) after 3ns;
          SLED <= temp_result(3 downto 0);    
        Cout <= temp_result(4) after 2ns;
    end process;
end Behavioral;