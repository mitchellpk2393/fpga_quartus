LIBRARY ieee;
 -- USE ieee.std_logic_vector.ALL;
USE ieee.std_logic_1164.ALL;
--use ieee.std_logic_signed.all;
use ieee.numeric_std.all; -- needed for binair optelling _ datatypes unsigned
ENTITY test1 is
 port(SW : in std_logic_vector(9 downto 2); -- init switches array 9 to 2 as input
        HEX4 : out std_logic_vector(6 downto 0); -- init dis hex4 as an out
        HEX2 : out std_logic_vector(6 downto 0);-- init dis hex2 as an out
		  HEX0 : out std_logic_vector(6 downto 0);-- init dis hex0 as an out
		  HEX1 : out std_logic_vector(6 downto 0);-- init dis hex0 as an out
        LEDR:out std_logic_vector(4 downto 0) -- init 5 leds init as ouput
);
    
end test1;
     
ARCHITECTURE behavior of test1 is
 -- begin function
function show_to_seg (
            -- declare func input are
            signal input : std_logic_vector(3 downto 0)
				)
        -- function output is type array name segment[7]
    return std_logic_vector is variable Segment : std_logic_vector(6 downto 0);
begin 
        case input is 
            -- 0  turn on seg, 1 turn off seg/ convert switch input to segment
            when "0000" => Segment := "1000000"; -- show 0
            when "0001" => Segment := "1111001"; -- show 1
            when "0010" => Segment := "0100100"; -- show 2
            when "0011" => Segment:= "0110000"; -- show 3 
            when "0100" => Segment := "0011001"; -- show 4
            when "0101" => Segment := "0010010"; -- show 5
            when "0110" => Segment := "0000010"; -- show 6
            when "0111" => Segment := "1111000"; -- show 7
            when "1000" => Segment := "0000000"; -- show 8
            when "1001" => Segment := "0010000"; -- show 9
            when others => Segment := "0001110";  -- show F error
        end case;
		  return Segment;
end show_to_seg; 


function show_2digits (
            -- declare func input are
            signal nummer : std_logic_vector(3 downto 0)
				)
        -- function output is type array name segment[7]
    return std_logic_vector is variable Segment : std_logic_vector(6 downto 0);
begin  
        CASE nummer IS 
            WHEN "0000" =>
                HEX0 <= "1000000"; -- show 0
                HEX1 <= "1111111";
            WHEN "0001" =>
                HEX0 <= "1111001"; -- show 1
                HEX1 <= "1111111";
            WHEN "0010" =>
                HEX0 <= "0100100"; -- show 2
                HEX1 <= "1111111";
            WHEN "0011" =>
                HEX0 <= "0110000"; -- show 3 
                HEX1 <= "1111111";
            WHEN "0100" =>
                HEX0 <= "0011001"; -- show 4
                HEX1 <= "1111111";
            WHEN "0101" =>
                HEX0 <= "0010010"; -- show 5
                HEX1 <= "1111111";
            WHEN "0110" =>
                HEX0 <= "0000010"; -- show 6
                HEX1 <= "1111111";
            WHEN "0111" =>
                HEX0 <= "1111000"; -- show 7
                HEX1 <= "1111111";
            WHEN "1000" =>
                HEX0 <= "0000000"; -- show 8
                HEX1 <= "1111111";
            WHEN "1001" =>
                HEX0 <= "0010000"; -- show 9
                HEX1 <= "1111111";
            WHEN "1010" =>
                HEX1 <= "1111001"; -- show 10
                HEX0 <= "1000000"; -- show 10
            WHEN "1011" =>
                HEX1 <= "1111001"; -- show 11
                HEX0 <= "1111001";
            WHEN "1100" =>
                HEX1 <= "1111001"; -- show 12
                HEX0 <= "0100100";
            WHEN "1101" =>
                HEX1 <= "1111001"; -- show 13
                HEX0 <= "0110000"; 
            WHEN "1110" =>
                HEX1 <= "1111001"; -- show 14
                HEX0 <= "0011001";
            WHEN "1111" =>
                HEX1 <= "1111001"; -- show 15
                HEX0 <= "0010010";
            WHEN OTHERS =>
                HEX0 <= "1111111"; -- show nothing
                HEX1 <= "1111111";
        END CASE;
        RETURN Segment;
END show_2digits;
	
-- signaal to compute calculations
signal SA : std_logic_vector(3 downto 0); -- var to store SW A
signal SB : std_logic_vector(3 downto 0); -- var to store SW B 
signal temp: std_logic_vector(3 downto 0); -- var to store optelling
signal Cout: std_logic_vector(4 downto 0); -- var to show 5 LEDS 5bits as Carryout
signal SA_resized : std_logic_vector(4 downto 0); -- var for 5bits optelling
signal SB_resized : std_logic_vector(4 downto 0); --var fot 5 bits optelling
signal error : std_logic_vector(6 downto 0);

begin
-- read input switch A & B, store value in var SB,SA
SB <= SW(5 downto 2);
SA <= SW(9 downto 6);

HEX2 <= show_to_seg(SB); -- show decimaal to segB
HEX4 <= show_to_seg(SA); -- show decimaal to segA


temp <= std_logic_vector(unsigned(SA) + unsigned(SB)); -- do optelling store result in temp
-- show decimaal + digit now
error <= show_2digits(temp); -- show decimaal optelling to segS
-- SA & SB MUST be resize to 5bits in order to get Cout Bit value,
SA_resized <= std_logic_vector(resize(unsigned(SA), Cout'length));
SB_resized <= std_logic_vector(resize(unsigned(SB), Cout'length));

-- Now that size is correct do bit optelling and store result and Carry out
Cout <= std_logic_vector(unsigned(SA_resized) + unsigned(SB_resized));

--4bits optelling + 1 bit Cout is send to LEDR as output
LEDR <= std_logic_vector(Cout);
-- in order to make 



end behavior;