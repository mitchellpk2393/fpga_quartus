library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--  Entity Declaration
ENTITY dataRegister8bit IS
    PORT
    (
        clk       : IN STD_LOGIC;
        NRST      : IN STD_LOGIC;
		  Din       : IN STD_LOGIC_VECTOR(7 downto 0);
		  control   : IN STD_LOGIC_VECTOR(3 downto 0);
		  enREG     : IN STD_LOGIC;
		  regOut0   : OUT STD_LOGIC;
		  regOut1   : OUT STD_LOGIC;
		  regOut2   : OUT STD_LOGIC;
		  regOut3   : OUT STD_LOGIC;
		  regOut4   : OUT STD_LOGIC;
		  regOut5   : OUT STD_LOGIC;
		  regOut6   : OUT STD_LOGIC;
		  regOut7   : OUT STD_LOGIC
	 
	     );
END dataRegister8bit;

-- KEY 1 ENGEN/ KEY 0 Enfast/ KEY3 RST/ KEY2 enReg / Control0 SW0 / Control1 SW1
ARCHITECTURE dataRegister8bit_architecture OF dataRegister8bit IS
SIGNAL led_data : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
SIGNAL test: unsigned(7 downto 0);
SIGNAL falling_edge_detected : STD_LOGIC := '0';

BEGIN
--PROCESS(control, enREG)
   -- BEGIN
	 
PROCESS(clk, NRST)
    BEGIN
        IF NRST = '0' THEN  -- Reset condition
            led_data <= (others => '0');
		  ELSIF RISING_EDGE(CLK) THEN  -- Rising edge of CLK
            IF EnREG = '1' THEN
					CASE CONTROL IS
                    WHEN "0001" =>  -- SW0 Store the led_data
                        -- Do nothing, keep the current led_data
								
                    WHEN "0010" =>  --SW1 Load new data
                        led_data <= Din
								after 10ns;  -- Load new 8-bit input data
								
                    WHEN "0100" =>  --SW2 Right shift one to right
                        --led_data <= '0' & led_data(7 downto 1)
								led_data <= std_logic_vector(unsigned(led_data) srl 1) after 10ns;  -- logical right shift
                    WHEN "1000" =>  --SW3 Bitwise invert
                        led_data <= std_logic_vector(NOT(unsigned(led_data)))
								after 10ns;  -- Bitwise invert of the led_data
								
                    WHEN OTHERS =>
                        -- Do nothing, default case
                END CASE;
				END IF;
        END IF;
		  
    END PROCESS;
	 

PROCESS(clk)
    BEGIN
        IF FALLING_EDGE(CLK) THEN
				 -- try to sync with 1 sec clock puls and then show output
				 Regout0 <= led_data(0);
				 Regout1 <= led_data(1);
				 Regout2 <= led_data(2);
				 Regout3 <= led_data(3);
				 Regout4 <= led_data(4);
				 Regout5 <= led_data(5);
				 Regout6 <= led_data(6);
				 Regout7 <= led_data(7);

        END IF;
END PROCESS;

END dataRegister8bit_architecture;
