library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--  Entity Declaration
ENTITY dataGenerator IS
    PORT
    (
        CLK   : IN STD_LOGIC;
        NRST  : IN STD_LOGIC;
        enGen : IN STD_LOGIC;
        enFast: IN STD_LOGIC;
        DOUT  : OUT STD_LOGIC_VECTOR(7 downto 0)
    );
END dataGenerator;

-- KEY 1 ENGEN/ KEY 0 Enfast/ KEY3 RST
--  Architecture Body
ARCHITECTURE dataGenerator_architecture OF dataGenerator IS
    SIGNAL counter : UNSIGNED(7 downto 0) := (others => '0');
BEGIN
    PROCESS(CLK, NRST)
    BEGIN
        IF NRST = '0' THEN  -- Reset condition
            DOUT <= (others => '0');  -- Clear DOUT
            counter <= (others => '0');  -- Reset counter
        ELSIF RISING_EDGE(CLK) THEN  -- Rising edge of CLK
            IF enGen = '0' THEN  -- Counting disable
                    IF enFast = '1' THEN
                    counter <= counter + 16;  -- Increment counter by 16
                ELSE
                    counter <= counter + 1;   -- Increment counter by 1
               END IF;
            END IF;
                    DOUT <= std_logic_vector(counter);  -- Output counter value to DOUT
        END IF;
    END PROCESS;