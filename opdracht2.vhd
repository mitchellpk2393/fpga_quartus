LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

ENTITY led is
 port(SW : in std_logic_vector(1 downto 0); -- init switch 1 and 0
		  HEX0 : out std_logic_vector(6 downto 0);-- init dis hex0 as an out
		  HEX1 : out std_logic_vector(6 downto 0);-- init dis hex0 as an out
		  KEY:in std_logic_vector(3 downto 0); -- init push butt, clk, Reset
		  LEDR:out std_logic_vector(1 downto 0) -- 2 debug leds init as ouput
);
END led;
     
ARCHITECTURE behavior OF led IS
  SIGNAL MEM_shift : std_logic_vector(3 DOWNTO 0) := "0000"; -- MEMORY to be shifted
  SIGNAL zeros     : std_logic_vector(3 DOWNTO 0) := "0000";
  SIGNAL error     : std_logic_vector(6 DOWNTO 0);
  SIGNAL ELF : std_logic_vector(3 downto 0) := "1011";

  --SIGNAL push_0    : std_logic; -- var for rising edge
  --SIGNAL push_1    : std_logic;
  SIGNAL PUSH : std_logic;
  
  -- function for show_2digits
  function show_2digits (
            -- declare func input are
            nummer : std_logic_vector(3 downto 0)
				)
        -- function output is type array name segment[7]
    return std_logic_vector is variable Segment : std_logic_vector(6 downto 0);
begin 
        CASE nummer IS 
            WHEN "0000" =>
                HEX0 <= "1000000"; -- show 0
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0001" =>
                HEX0 <= "1111001"; -- show 1
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0010" =>
                HEX0 <= "0100100"; -- show 2
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0011" =>
                HEX0 <= "0110000"; -- show 3 
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0100" =>
                HEX0 <= "0011001"; -- show 4
                HEX1 <= "1111111";
            WHEN "0101" =>
                HEX0 <= "0010010"; -- show 5
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0110" =>
                HEX0 <= "0000010"; -- show 6
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "0111" =>
                HEX0 <= "1111000"; -- show 7
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "1000" =>
                HEX0 <= "0000000"; -- show 8
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "1001" =>
                HEX0 <= "0010000"; -- show 9
                HEX1 <= "1111111";
					 Segment := "1111111";
            WHEN "1010" =>
                HEX1 <= "1111001"; -- show 10
                HEX0 <= "1000000"; -- show 10
            WHEN "1011" =>
                HEX1 <= "1111001"; -- show 11
                HEX0 <= "1111001";
					 Segment := "1111111";
            WHEN "1100" =>
                HEX1 <= "1111001"; -- show 12
                HEX0 <= "0100100";
					 Segment := "1111111";
            WHEN "1101" =>
                HEX1 <= "1111001"; -- show 13
                HEX0 <= "0110000";
					 Segment := "1111111";
            WHEN "1110" =>
                HEX1 <= "1111001"; -- show 14
                HEX0 <= "0011001";
					 Segment := "1111111";
            WHEN "1111" =>
                HEX1 <= "1111001"; -- show 15
                HEX0 <= "0010010";
					 Segment := "1111111";
            WHEN OTHERS =>
                HEX1 <= "1111111"; -- show nothing
                HEX0 <= "1111111";
					 Segment := "1111111";
        END CASE;
        RETURN Segment;
END show_2digits;

BEGIN -- architecture


  --push_0 <= KEY(0);
  --push_1 <= KEY(1);
 
PROCESS(KEY)
  VARIABLE shift_temp : std_logic_vector(3 DOWNTO 0); -- declare a variable
BEGIN
  PUSH <= KEY(2) OR KEY(3);
  --IF(KEY(2) = '1' or KEY(3) = '1')THEN -- this is 2chance, dlt or comment other IF statement
  IF((KEY(2) = '1' OR KEY(3) = '1') AND RISING_EDGE(PUSH))THEN -- if not working use 2chance
		LEDR(0) <= '0'; -- Set LEDS to low
		LEDR(1) <= '0';
		
		IF (KEY(3) = '1') THEN -- == KEY1 on board
		  LEDR(0) <= '1';
		  shift_temp := (MEM_shift(2 DOWNTO 0)) & (SW(0)); -- assign value to variable
		  MEM_shift <= shift_temp;  -- update the signal
		ELSIF (KEY(2) = '1') THEN -- == KEY0 on board
		  LEDR(1) <= '1';
		  MEM_shift <= zeros; -- assign all zeros to MEM_shift
		END IF;
		
		error <= show_2digits(MEM_shift); -- assign the result of show_2digits to the signal error
	END IF;

END PROCESS;



  
END behavior;
