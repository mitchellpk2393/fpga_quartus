library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity TOSignMagnitude is
    Port (
        S      : in  STD_LOGIC_VECTOR (3 downto 0);
        Cobo   : in  STD_LOGIC;
        SEL    : in  STD_LOGIC_VECTOR(1 downto 0);
        Output : out STD_LOGIC_VECTOR (3 downto 0);
        OVF    : out STD_LOGIC;
        Sign   : out STD_LOGIC;
        MIN    : out STD_LOGIC_VECTOR (3 downto 0)
    );
end TOSignMagnitude;

architecture Behavioral of TOSignMagnitude is
begin
    process(S, Cobo, SEL)
        variable sum : STD_LOGIC_VECTOR (3 downto 0);       -- Temporary variable for sum
        variable is_negative : STD_LOGIC;                   -- Flag to indicate negative sign
    begin
        if SEL = "00" then                                   -- Check if it's an addition
            sum := S;                                       -- No modification for addition
            Sign <= '0'; 
        elsif SEL = "01" then
            sum := (not S) + "0001";                        -- Two's complement for subtraction   
            if sum = "0000" then
                Sign <= '0';
            else 
                Sign <= '1'; 
            end if;
        end if;

        if (SEL = "00" and Cobo = "0") or (SEL = "01" and Cobo = "1") then
            Output <= sum;                                  -- Output is the sum (unchanged in case of overflow)
            OVF <= '0';                                     -- No overflow
        else
            Output <= sum;
            OVF <= '1';                                     -- Overflow occurred
        end if;
    end process;
end Behavioral;
