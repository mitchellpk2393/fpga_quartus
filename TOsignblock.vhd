library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Entity Declaration

ENTITY TOHEXSIGN IS
    
    PORT
    (
        BIN : IN STD_LOGIC_VECTOR(3 downto 0);
        seg : OUT STD_LOGIC_VECTOR(6 downto 0)
    );
    

END TOHEXSIGN;


--  Architecture Body

ARCHITECTURE TOHEX_architecture OF TOHEXSIGN IS


BEGIN
    process (BIN)
    begin
		  if BIN = "1111" then
				seg <="0111111" after 1 ns; -- check if g seg turn on the middel
		  else
				seg <="1111111" after 1 ns;
		  
		  end if;
    end process;
END TOHEX_architecture;