library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Entity Declaration

ENTITY ToSignBlock IS
    
    PORT
    (
        BIN : IN STD_LOGIC;
        Dis2 : OUT STD_LOGIC_VECTOR(6 downto 0)
    );
    

END ToSignBlock;


--  Architecture Body

ARCHITECTURE TOHEX_architecture OF ToSignBlock IS


BEGIN
    process (BIN)
    begin
          if BIN = '1' then
                Dis2 <="0111111" ; -- check if g seg turn on the middel
          else
                Dis2 <="1111111" ;
          
          end if;
    end process;
END TOHEX_architecture;
